/** @file
  ArmCcaDxe

  Copyright (c) 2022 - 2023, ARM Ltd. All rights reserved.<BR>
  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#include <Base.h>
#include <Library/ArmCcaLib.h>
#include <Library/ArmCcaRsiLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/HobLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>

/** Entrypoint of Arm CCA Dxe.

  @param [in] ImageHandle   Image handle of this driver.
  @param [in] SystemTable   Pointer to the EFI System Table.

  @retval RETURN_SUCCESS               Success.
  @retval EFI_NOT_FOUND                Required HOB not found.
**/
EFI_STATUS
EFIAPI
ArmCcaDxe (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS  Status;

  if (!IsRealm ()) {
    // Nothing to do here, return SUCCESS.
    return EFI_SUCCESS;
  }

  // Setup the conduit to be used by Realm code to SMC.
  Status = PcdSetBoolS (PcdMonitorConduitHvc, FALSE);
  if (EFI_ERROR (Status)) {
    DEBUG ((DEBUG_ERROR, "ERROR - Failed to set PcdMonitorConduitHvc\n"));
    ASSERT (0);
    return Status;
  }

  return Status;
}
